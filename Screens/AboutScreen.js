import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, Button, ScrollView, Dimensions } from 'react-native'

const DEVICE = Dimensions.get('window')

export default class App extends Component {
  render() {
    const { navigation } = this.props;

    return (
      <ScrollView style={styles.container}>
        <Text style={styles.pageTitle}>About Me</Text>

        <View style={styles.userInfoContainer}>
          <View style={styles.userPhoto}>
            <Image
              source={require('./images/photo.jpg')}
              style={{width: 200, height: 200, borderRadius: 100}}
            />
          </View>         

          <View>
            <Text style={styles.username}>Erwan Ari</Text>
          </View>

          <View style={styles.textCenter}>
            <Text style={styles.whiteText}>Build with Expo</Text>
            <Text style={styles.whiteText}>API https://jikan.moe/</Text>
          </View> 
        </View>

        {/* <View style={{marginVertical: 16, padding: 16}}>
          <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
        </View>         */}

      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1f1f1f',
    paddingVertical: 16,
  },
  pageTitle: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 16,
  },
  whiteText: {
    color: 'white',
  },
  textCenter: {
    textAlign: 'center',
  },
  userInfoContainer: {
    padding: 20,
  },
  userPhoto: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  username: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingBottom: 16,
  },
  userInfo: {
    color: '#3EC6FF',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingBottom: 8,
  },
  box: {
    backgroundColor: '#EFEFEF',
    marginHorizontal: 16,
    padding: 16,
    borderRadius: 12,
    marginBottom: 16,
  },
  boxTitle: {
    color: '#003366',
  },

  portofolioBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  portofolioItem: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
  },
  portofolioTitle: {
    color: '#003366',
    paddingTop: 4,
    fontWeight: 'bold',
  }, 

  contactBox: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 'auto',
    justifyContent: 'center',
    paddingVertical: 8,
  },
  contactTitle: {
    color: '#003366',
    fontWeight: 'bold',
    width: 150,
    paddingLeft: 16,
  },   
})
