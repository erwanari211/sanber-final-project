import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, ScrollView, FlatList, Dimensions, Button } from 'react-native'
import {connect} from 'react-redux';

const DEVICE = Dimensions.get('window')

class HomeScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: []
    }
  }

  componentDidMount() { 
    this.fetchData()
  }

  fetchData = async () => {
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let year = dateObj.getUTCFullYear();
    let season = {}

    switch (month) {
      case 1:
        season = { season: 'winter', year: year};
        break;
      case 2:
        season = { season: 'winter', year: year};
        break;
      case 3:
        season = { season: 'spring', year: year};
        break;
      case 4:
        season = { season: 'spring', year: year};
        break;
      case 5:
        season = { season: 'spring', year: year};
        break;
      case 6:
        season = { season: 'spring', year: year};
        break;
      case 7:
        season = { season: 'summer', year: year};
        break;
      case 8:
        season = { season: 'summer', year: year};
        break;
      case 9:
        season = { season: 'summer', year: year};
        break;
      case 10:
        season = { season: 'summer', year: year};
        break;
      case 11:
        season = { season: 'winter', year: year + 1};
        break;
      case 12:
        season = { season: 'winter', year: year + 1};
        break;
    }

    // 2. Aplikasi terhubung dengan API
    const apiUrl = `https://api.jikan.moe/v3/season/${season.year}/${season.season}`
    const response = await fetch(apiUrl)
    const json = await response.json()
    this.setState({ data: json.anime })
  }

  render() {
    const { navigation } = this.props;

    return (
      <ScrollView style={styles.container}>
        <Text style={styles.pageTitle}>Anime List</Text>

        <View style={styles.welcome}>
          <Text style={styles.whiteText}>Welcome {this.props.username}</Text>
        </View>

        {/* <View style={{marginVertical: 16, padding: 16}}>
          <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
        </View> */}

        <FlatList
            data={this.state.data}
            numColumns={2}
            keyExtractor={(x,i) => `item-${i}`}
            renderItem={({item}) => (
              <AnimeListItem data={item} navigation={navigation} />
            )}
        />
      </ScrollView>
    )
  }
}

class AnimeListItem extends React.Component {
  render() {
    const data = this.props.data
    const { navigation } = this.props;

    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.image_url }} style={styles.itemImage} resizeMode='contain' />
        <TouchableOpacity onPress={ () => navigation.push('Anime', { mal_id: data.mal_id, name: data.title }) }>
          <Text numberOfLines={2} ellipsizeMode='clip' style={styles.itemName} >
            {data.title}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
};

// 5. Menggunakan Redux untuk state management
const mapStateToProps = state => {
  return {
    username: state.username,
  };
};

export default connect(mapStateToProps)(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1f1f1f',
    paddingVertical: 16,
  },

  welcome: {
    marginHorizontal: DEVICE.width * 0.04,
    marginBottom: 16,
  },

  itemContainer: {
    width: DEVICE.width * 0.4,
    backgroundColor: '#0d0d0d',
    marginBottom: 16,
    marginHorizontal: DEVICE.width * 0.04,
    padding: 16,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    borderRadius: 8,
  },
  itemImage: {
    width: DEVICE.width * 0.3,
    height: DEVICE.width * 0.5,
    marginHorizontal: 'auto',
  },
  itemName: {
    color: 'white',
    textAlign: 'center',
  },

  pageTitle: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 16,
  },
  whiteText: {
    color: 'white',
  },
})
