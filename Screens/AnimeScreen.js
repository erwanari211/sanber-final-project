import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, ScrollView, Dimensions } from 'react-native'

const DEVICE = Dimensions.get('window')

export default class AnimeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        genres: []
      }
    }
  }

  // Mount User Method
  componentDidMount() { 
    this.fetchData()
  }

  fetchData = async () => {
    const { route } = this.props;
    // 2. Aplikasi terhubung dengan API
    const apiUrl = `https://api.jikan.moe/v3/anime/${route.params.mal_id}`
    const response = await fetch(apiUrl)
    const json = await response.json()

    this.setState({ data: json })
  }

  render() {
    const { data } = this.state;

    return (
      <ScrollView style={styles.container}>
        <Text style={styles.pageTitle}>Anime Detail</Text>

        <View style={styles.itemContainer}>
          <Image source={{ uri: data.image_url }} style={styles.itemImage} resizeMode='contain' />

          <Text style={styles.animeTitle}>{data.title}</Text> 
          <Text style={styles.animeSynopsis}>Synopsis: {data.synopsis}</Text> 

          <Text style={styles.whiteText}>Information</Text> 
          <Text style={styles.whiteText}>Type: {data.type}</Text> 
          <Text style={styles.whiteText}>Episode: {data.episodes}</Text> 
          <Text style={styles.whiteText}>Source: {data.source}</Text> 
          <Text style={styles.whiteText}>Genres: {data.genres.map((item) => `${item.name} ` )}</Text> 
        </View>        
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1f1f1f',
    padding: 16,
  },

  itemContainer: {
    width: DEVICE.width * 0.8,
    backgroundColor: '#0d0d0d',
    marginBottom: 16,
    marginHorizontal: DEVICE.width * 0.04,
    padding: 16,
    shadowColor: '#333',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    borderRadius: 8,
  },
  itemImage: {
    width: DEVICE.width * 0.3,
    height: DEVICE.width * 0.5,
    marginHorizontal: 'auto',
  },
  itemName: {
    color: 'white',
    textAlign: 'center',
  },
  animeTitle: {
    color: 'white',
    fontSize: 20,
    marginBottom: 8,
  },
  animeSynopsis: {
    color: 'white',
    marginBottom: 8,
  },

  pageTitle: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 16,
  },
  whiteText: {
    color: 'white',
  },
})
