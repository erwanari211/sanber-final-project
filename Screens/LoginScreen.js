import React, { Component, useState } from 'react'
import { View, StyleSheet, TouchableOpacity, Text, TextInput, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

export default function LoginScreen({ navigation }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch()

  const onSubmitForm = () => {
    if(username.trim() === '' || password.trim() === ''){
      alert('Invalid username or password')
    } else {
      // 5. Menggunakan Redux untuk state management
      dispatch({ type: 'LOGIN', username: username })
      navigation.reset({ index: 0, routes: [{ name: 'Drawer' }] })
    }
  }

  return (
    <ScrollView style={styles.container}>
      <View>
        <Text style={styles.pageTitle}>Login</Text>

        <View style={styles.form}>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Username</Text>
            <TextInput style={styles.textInput}
              onChangeText={val => setUsername(val)}
            />
          </View>

          <View style={styles.formGroup}>
            <Text style={styles.label}>Password</Text>
            <TextInput style={styles.textInput}
              secureTextEntry={true}
              onChangeText={val => setPassword(val)}
            />
          </View>

          <View style={styles.formGroup}>
            <TouchableOpacity onPress={ onSubmitForm }>
              <Text style={styles.loginButton}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </ScrollView>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1f1f1f',
    padding: 16,
  },
  logo: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  pageTitle: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
  form: {
    padding: 24,
  },
  formGroup: {
    paddingBottom: 16,
  },
  label: {
    color: 'white',
    paddingBottom: 4,
  },
  textInput: {
    height: 40,
    borderColor: 'white',
    backgroundColor: '#4d4d4d',
    color: 'white',
    borderWidth: 1,
    padding: 10,
  },
  registerButton: {
    backgroundColor: '#ef3862',
    padding: 12,
    textAlign: 'center',
    color: 'white',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: '#ed2553',
    padding: 12,
    textAlign: 'center',
    color: 'white',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  
})
