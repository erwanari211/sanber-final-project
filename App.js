import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './components/reducers'
const store = createStore(reducer)

import { LoginScreen, HomeScreen, AnimeScreen, AboutScreen } from './Screens/index'

const RootStack = createStackNavigator();

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    {/* 4. Halaman Utama */}
    <Drawer.Screen name="Tabs" component={HomeScreen} options={{ title: 'Home' }} />
    {/* 4. Halaman About Me */}
    <Drawer.Screen name="About" component={AboutScreen} options={{ title: 'About Me' }} />   
  </Drawer.Navigator>
)

// 1. Aplikasi dibangun menggunakan Expo CLI
export default function App() {
  return (
    <Provider store={store}>
      {/* 3. Menggunakan React Navigation 5 untuk navigasi antar Screen */}
      <NavigationContainer>
        <RootStack.Navigator>
          {/* 4. Halaman Login */}
          <RootStack.Screen
            name="Login"
            component={LoginScreen}
            options={{ title: 'Login' }}
          />
          {/* 4. Halaman Utama */}
          <RootStack.Screen
            name="Drawer"
            component={DrawerScreen}
            options={{ title: 'Home' }}
          />
          {/* 4. Halaman Detail */}
          <RootStack.Screen
            name="Anime"
            component={AnimeScreen}
            options={({ route }) => ({
              title: route.params.name
            })}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </Provider>    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
