import { LOGIN } from './types';

// initial state
const initialState = {
    username: ''
};

function reducer(state = initialState, action){
    switch (action.type) {
        case LOGIN:
            state.username = action.username;
            return state;
        default:
            return state;
    }
}

export default reducer;